# Checking out the rust sort

For a vector of 10000000 elements:

Stable sort finishes in...
```
*****
Vector generated of size: 10000000 in 0.127 seconds
Sorted!
Time spent sorting: 0.864
*****
Vector generated of size: 10000000 in 0.126 seconds
Sorted!
Time spent sorting: 0.857
*****
Vector generated of size: 10000000 in 0.125 seconds
Sorted!
Time spent sorting: 0.847
```

Unstable sort finishes in...
```
*****
Vector generated of size: 10000000 in 0.131 seconds
Sorted!
Time spent sorting: 0.413
*****
Vector generated of size: 10000000 in 0.128 seconds
Sorted!
Time spent sorting: 0.465
*****
Vector generated of size: 10000000 in 0.126 seconds
Sorted!
Time spent sorting: 0.415
*****
Vector generated of size: 10000000 in 0.127 seconds
Sorted!
Time spent sorting: 0.415
*****

```
