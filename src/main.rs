use rand;
use rand::Rng;
use std::time::{SystemTime, UNIX_EPOCH};

fn main() {
    loop {
        let gen_start = millis_since_unix();
        let mut vals: Vec<u32> = (0..10000000)
            .map(|_| rand::thread_rng().gen_range(0, 2147483647))
            .collect();
        let gen_end = millis_since_unix();
        println!("*****");
        println!(
            "Vector generated of size: {} in {} seconds",
            vals.len(),
            (gen_end - gen_start) as f64 / 1000.0
        );

        let start_millis = millis_since_unix();
        vals.sort_unstable();
        let end_millis = millis_since_unix();
        println!("Sorted!");
        println!(
            "Time spent sorting: {}",
            (end_millis - start_millis) as f64 / 1000.0
        );
    }
}

fn millis_since_unix() -> u128 {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    since_the_epoch.as_millis()
}
